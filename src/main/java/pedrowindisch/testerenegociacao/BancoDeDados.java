package pedrowindisch.testerenegociacao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BancoDeDados {
	private Connection instancia;
	
	public BancoDeDados(String urlConexao) throws SQLException {
		instancia = DriverManager.getConnection(urlConexao);
	}
	
	public ResultSet executarConsulta(String consulta, Object[] parametros) throws SQLException {
		PreparedStatement ps = instancia.prepareStatement(consulta);
		
		if(parametros != null) {
			int indice = 1;
			
			for(Object parametro : parametros) {
				ps.setObject(indice, parametro);;
			}
		}
		
		return ps.executeQuery();
	}
}