package pedrowindisch.testerenegociacao.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import pedrowindisch.testerenegociacao.BancoDeDados;

public class Renegociacao {
	private String descricao;
	private String codigo;
	private String data;
	
	public ArrayList<Parcela> getParcelasOrigem(BancoDeDados banco) throws SQLException {
		String consulta = "SELECT * FROM renegociacao_parcela AS rnp "
				        + "WHERE rnp.RNP_TIPO = \"ORIGEM\" AND rnp.RNP_RENID = ?"
				        + "GROUP BY rnp.RNP_PARID";

		Object[] parametrosConsulta = { this.codigo };
		ResultSet resultado = banco.executarConsulta(consulta, parametrosConsulta);
		
		ArrayList<Parcela> parcelasOrigem = new ArrayList<Parcela>();
		
		while(resultado.next()) {
			Parcela parcelaAtual = new Parcela(resultado.getInt("RNP_PARID"));
			parcelasOrigem.add(parcelaAtual);
		}
		
		return parcelasOrigem;
	}
	
	public Renegociacao(String codigo, BancoDeDados banco) throws SQLException {
		this.codigo = codigo;
		
		String consulta = "SELECT * FROM renegociacao AS ren WHERE ren.REN_ID = ?";
		Object[] parametrosConsulta = { codigo };
		
		ResultSet resultado = banco.executarConsulta(consulta, parametrosConsulta);
		resultado.next();
		
		this.descricao = resultado.getString("REN_DESCRI");
		this.data = resultado.getString("REN_DATA");
	}
	
	public String gerarMensagem(int indentacao) {
		return "\t".repeat(indentacao) + String.format("%s - %s (%s) \n", this.codigo, this.descricao, this.data);
	}
}
