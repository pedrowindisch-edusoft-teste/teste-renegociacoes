package pedrowindisch.testerenegociacao.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import pedrowindisch.testerenegociacao.BancoDeDados;

public class Parcela {
	private int codigo;
	
	public Renegociacao encontrarRenegociacaoOrigem(BancoDeDados banco) throws SQLException {
		String consulta = "SELECT ren.* FROM renegociacao AS ren "
						+ "INNER JOIN renegociacao_parcela AS rnp "
						+ "ON rnp.RNP_TIPO = \"DESTINO\" "
						+ "AND rnp.RNP_PARID = ? "
						+ "AND rnp.RNP_RENID = ren.REN_ID "
						+ "GROUP BY ren.REN_ID ";
						
		Object[] parametrosConsulta = { this.codigo };
		ResultSet resultado = banco.executarConsulta(consulta, parametrosConsulta);
		
		resultado.next();
		
		Renegociacao renegociacaoOrigem = new Renegociacao(resultado.getString("REN_ID"), banco);
		
		return renegociacaoOrigem;
	}
	
	public Parcela(int codigo) {
		this.codigo = codigo;
	}
}
