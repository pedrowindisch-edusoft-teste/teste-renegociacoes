package pedrowindisch.testerenegociacao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import pedrowindisch.testerenegociacao.models.Parcela;
import pedrowindisch.testerenegociacao.models.Renegociacao;

public class App {
	private static final String urlConexaoBanco = "jdbc:sqlite:dados.db";
	private static String resultadoFinal = "";
	private static BancoDeDados banco;
	
	public static void encontrarOrigemDeParcelas(ArrayList<Parcela> parcelas, int indentacao) throws SQLException {
		for(Parcela parcela : parcelas) {
			Renegociacao origem = parcela.encontrarRenegociacaoOrigem(banco);			
			resultadoFinal += origem.gerarMensagem(indentacao);
			
			encontrarOrigemDeParcelas(origem.getParcelasOrigem(banco), indentacao + 1);
		}
	}
	
	public static void main(String[] args) throws SQLException {
		banco = new BancoDeDados(urlConexaoBanco);
		
		String idRenegociacaoFornecido = args[0];
		Renegociacao renegociacao = new Renegociacao(idRenegociacaoFornecido, banco);
		
		resultadoFinal += renegociacao.gerarMensagem(0);
		resultadoFinal += "Renegociações de Origem \n";
		
		encontrarOrigemDeParcelas(renegociacao.getParcelasOrigem(banco), 1);
		
		System.out.println(resultadoFinal);
    }
}
